import numpy as np
from tqdm import tqdm
class HIO:
    def __init__(self, plane, beta = 0.99):
        self.plane = plane
        self.grating = np.abs(np.fft.ifft2(plane))
        self.beta = beta

        x0 = 0#reconstruction.shape[0]/2  # x center, half width                                       
        y0 = 0#reconstruction.shape[1]/2 
        b = 175
        a = 101  # y center, half height                                      
        x = np.linspace(-plane.shape[0]/2, plane.shape[0]/2, plane.shape[0])  # x values of interest
        y = np.linspace(-plane.shape[1]/2, plane.shape[1]/2, plane.shape[1])[:,None]  # y values of interest, as a "column" array
        self.ellipse = (((x-x0)/a)**2 + ((y-y0)/b)**2 <= 1).T  # True for points inside the ellipse

    def run(self, iter, cycles):
        grating = self.grating
        for cycle in tqdm(range(cycles)):
            for i in range(iter*2):
                plane_est = np.fft.fft2(grating)
                plane_est_phase = np.angle(plane_est)
                plane_est_corr = self.plane * np.exp(plane_est_phase * 1j)
                grate_corr = np.fft.ifft2(plane_est_corr)
                grate_corr = np.abs(grate_corr)
                # mx, mn = np.max(np.abs(grate_corr)), np.min(np.abs(grate_corr))
                # cond = ( (np.abs(grate_corr) > mn + (mx-mn) * 0.6) | (np.abs(grate_corr) < mn + (mx-mn) * 0.4))
                # cond = self.cond_f(grate_corr)
                # grate_corr_add = np.copy(grate_corr)
                # grate_corr_add[410:710,775:1125] = 0
                # gr_add = np.copy(grating)
                # gr_add[410:710,775:1125] = 0
                # grating = (grate_corr - grate_corr_add) + (gr_add - self.beta*grate_corr_add) #* (np.bitwise_not(cond))
                assert grate_corr.shape == self.ellipse.shape, "Fuck"
                grating = (grate_corr * self.ellipse) + ((grating - self.beta*grate_corr) * np.bitwise_not(self.ellipse))
            for i in range(iter):
                plane_est = np.fft.fft2(grating)
                plane_est_phase = np.angle(plane_est)
                plane_est_corr = self.plane * np.exp(plane_est_phase * 1j)
                grate_corr = np.fft.ifft2(plane_est_corr)
                grate_corr = np.abs(grate_corr)
                grating = (grate_corr * self.ellipse)
        self.grating = grating
        return grating