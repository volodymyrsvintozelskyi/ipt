import numpy as np
import scipy.misc
import matplotlib.pyplot as plt
import matplotlib as mtplb

rec = np.load("./reconstructed_1_6M_fixed.npy")
print(rec)
rec /= np.max(np.abs(rec))
print(np.min(np.abs(rec)),np.max(np.abs(rec)))

plt.imshow(np.abs(( rec)))#, norm=mtplb.colors.LogNorm(vmin=np.min(np.abs(rec)), vmax=np.max(np.abs(rec))))
plt.show()
