# %%
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib as mtplb
import scipy.ndimage
import numpy as np
from tqdm import tqdm
from PIL import Image

data = np.load("data.npy")
data = np.array(Image.fromarray(data).resize((350,200)))
print(data.shape)

grating = np.random.rand(*data.shape)
mask = np.zeros_like(data)
mask[20:-20, 20:-20] = 1
mask = np.fft.fftshift(mask)

hio, er = 400, 0
beta = 0.95
for iter in tqdm(range(1600000)):
    image = np.fft.fft2(grating)
    image = np.abs(np.fft.fftshift(data)) * np.exp(1j * np.angle(image))
    corr_gr = np.fft.ifft2(image)
    if er > 0:
        corr_gr = corr_gr * mask
        er -= 1
        if er == 0: 
            hio = 400
    if hio > 0:
        corr_gr = corr_gr * mask + (1-mask) * (grating - beta * corr_gr)
        hio -= 1
        if hio == 0: 
            er = 200
    grating = np.abs(corr_gr)
    if (iter % (600*2)) == 0 and iter != 0:
        smearing = scipy.ndimage.filters.gaussian_filter(np.abs(grating), 1.5, mode="nearest")
        smearing -= np.min(smearing)
        smearing /= np.max(np.abs(smearing))
        mask = (smearing > 0.03).astype(int)
        # print("Smearing: ", np.min(smearing), np.max(smearing), "   MASK  ", np.max(mask), np.min(mask))
grating /= np.max(np.abs(grating))

np.save("reconstructed_1_6M", grating)



