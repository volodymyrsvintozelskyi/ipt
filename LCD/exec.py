import numpy as np
import scipy.misc

cutted = np.load("data.npy")

from hio import *
hio = HIO(cutted)
hio.grating = hio.grating * np.exp(2j*np.pi * np.random.uniform(size = hio.grating.shape))
reconstruction = np.fft.fftshift( hio.run(200,12))
np.save("reconstruction", reconstruction)