import numpy as np
import scipy.misc

rec = np.load("reconstruction.npy")
plt.imshow(np.fft.fftshift( rec))
plt.show()
