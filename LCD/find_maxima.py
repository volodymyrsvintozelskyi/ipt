import numpy as np
import scipy
import scipy.ndimage as ndimage
import scipy.ndimage.filters as filters
import matplotlib.pyplot as plt

fname = 'test.png'
neighborhood_size = 10
threshold = 0.0025

data = np.abs(np.load("reconstruction.npy"))

data_max = filters.maximum_filter(data, neighborhood_size)
maxima = (data == data_max)
data_min = filters.minimum_filter(data, neighborhood_size)
diff = ((data_max - data_min) > threshold)
maxima[diff == 0] = 0

labeled, num_objects = ndimage.label(maxima)
slices = ndimage.find_objects(labeled)
x, y = [], []
for dy,dx in slices:
    x_center = (dx.start + dx.stop - 1)/2
    x.append(x_center)
    y_center = (dy.start + dy.stop - 1)/2    
    y.append(y_center)

# print(x,y)
import matplotlib as mtplb
plt.imshow(data, norm=mtplb.colors.LogNorm(vmin=np.min(data*10), vmax=np.max(data)))
# plt.scatter(x,y)

maximums = np.vstack([x,y])
maximums[0,:] -= np.min(maximums[0,:])
# maximums[1,:] -= np.min(maximums[1,:])
print(maximums.shape)

print(maximums)

plt.show()
# plt.imshow(data)
# plt.savefig('/tmp/data.png', bbox_inches = 'tight')

# plt.autoscale(False)
# plt.plot(x,y, 'ro')
# plt.savefig('/tmp/result.png', bbox_inches = 'tight')