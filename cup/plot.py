import matplotlib.pyplot as plt
import numpy as np
import os

# degrees = ["0", "22.5", "45", "77.5", "90", "112.5", "135", "157.5", "180", "202.5","225", "247.5", "270", "292.5", "315", "337.5"]
degrees = ["0", "22.5", "45", "77.5", "90"]
spectras = list(range(1, 5))
counter = 0
plt.rcParams['font.size'] = 5
folder = "big_cup_phone"
# folder = "first_cup"
fig, ax = plt.subplots(1)


for degree in degrees:
	path = folder + "/degree" + str(degree) + "/spectrum" + str(1)+".txt"
	x, y = [], []
	# if(os.path.exists(path)):
	with open(path , "r") as file:
		lines = file.readlines()
		for i in range(1, len(lines)):
			line = lines[i].split()
			x.append(float(line[0]))
			y.append(float(line[1]))
	ax.plot(x,y, label=str(degree))
		
legend = ax.legend(loc='upper right', shadow=True, fontsize='x-large')
plt.show()	