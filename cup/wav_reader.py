import matplotlib
import matplotlib.pyplot as plt
import scipy.fft
from scipy.io import wavfile
from scipy.optimize import curve_fit
import numpy as np
folder = "orange_10deg"
# samplerate, data = wavfile.read("orange_cup_micro_inside.wav")
samplerate, data = wavfile.read("./cup/cup_2ruchki.wav")
primary_channel = 1
print(data.shape)

fig,ax = plt.subplots(1)
ax.plot(np.arange(data.shape[0]), data[:,primary_channel])
# for phone audio
# ax.plot(np.arange(data.shape[0]), data)

def found_beginning(data, start = 0, level=10000):
	for i,el in enumerate(data[start:]):
		if np.abs(el) > level:
			return start + i
	raise Exception("Not found error")

def Gauss(x, A, mu, sigma, b, c):    
	return A*np.exp((-1*((x-mu)/sigma)**2)/2)+b*x+c

def smoothing(arr, n):
	sum = np.sum(arr[0: n])
	for i in range(n//2, arr.size - n//2):
		arr[i] = sum/n
		if(i!= arr.size-n//2-1): sum += (-arr[i - n//2]+arr[n+i - n//2])



# red_cup_points = np.round([0,1.68e5,3.4e5,5.01e5,6.65e5,8.26e5,9.9e5, 1.13e6, 1.28e6, 1.45e6, 1.6e6, 1.76e6, 1.93e6, 2.19e6, 2.41e6, 2.66e6])
# big_cup_points_phone = np.round([0,2.5e5,4.6e5,6.8e5,8.75e5,1.07e6, 1.29e6, 1.5e6, 1.7e6, 1.91e6, 2.13e6, 2.34e6, 2.54e6, 2.76e6, 2.97e6, 3.17e6])
# small_cup_points = np.round([0,3.1e5,5.5e5,8.5e5,1.13e6,1.42e6, 1.61e6, 1.83e6, 2.03e6, 2.30e6, 2.61e6, 2.91e6, 3.1e6, 3.3e6, 3.5e6, 3.75e6])
# orange10_points = np.round([0,1.84e5, 4.1e5, 5.7e5, 7e5, 8.1e5, 9.6e5, 1.07e6, 1.21e6, 1.33e6, 1.45e6, 1.57e6, 1.68e6, 1.79e6, 1.90e6, 2.04e6, 2.16e6,2.30e6, 2.44e6, 2.56e6, 2.68e6, 2.82e6, 2.95e6, 3.08e6, 3.21e6, 3.35e6, 3.48e6, 3.63e6, 3.75e6, 3.85e6, 3.96e6, 4.07e6, 4.18e6, 4.32e6, 4.45e6, 4.58e6, 4.75e6])
# orange10_points_micro_inside = np.round([0,80000, 140000, 200000, 280000, 340000, 400000, 460000, 520000, 600000, 650000, 700000, 760000, 820000, 856000, 910000, 1000000,1075000,1135000 , 1183000, 1225000, 1269000, 1333000, 1374000, 1456000, 1523000, 1553000, 1606000, 1658000, 1739000, 1800000, 1865000, 1929000, 2004000, 2060000, 2122000, 2174000])
cup_2ruchki_points = np.round([0,140000, 270000, 385000, 485000, 578000, 694000, 831000, 933000, 1.045e6, 1.147e6, 1.246e6, 1.371e6, 1.49e6, 1.653e6, 1.79e6 , 1.941e6, 2.111e6, 2.275e6, 2.431e6, 2.59e6, 2.75e6, 2.95e6, 3.11e6, 3.265e6, 3.38e6, 3.47e6, 3.64e6, 3.84e6, 4.02e6, 4.26e6, 4.3e6, 4.45e6, 4.59e6, 4.74e6, 4.85e6, 5.0e6])
start_points = np.array(cup_2ruchki_points, dtype=int)
improved_start_points = np.array(list(map(lambda st: found_beginning(data[:,primary_channel],st), start_points)))
# improved_start_points = np.array(list(map(lambda st: found_beginning(data,st), start_points)))
end_points = np.round(improved_start_points + .3*samplerate).astype(int)
regions = np.vstack([improved_start_points,end_points])
region_size = (end_points - improved_start_points)[0]
print(regions.shape)

fig,ax = plt.subplots(nrows=regions.shape[1]//4+1 , ncols=4) 
fig1,ax1 = plt.subplots(nrows=regions.shape[1]//4+1 , ncols=4) 

# peaks =np.array([[4650, 4975, 10500, 11670], [4850, 5200, 10670, 11850]])
peaks =np.array([[1075, 1325, 2900, 3400], [1325, 1625, 3100, 3600]])
spectrum = np.zeros([regions.shape[1], region_size//2])
spectrum_fit_param = np.zeros((regions.shape[1], peaks.shape[1], 5))
# koef = 44100/region_size
koef = 44100/region_size
x = np.arange(0,region_size//2)*koef

for reg in range(regions.shape[1]): 
	subdata = data[regions[0,reg]:regions[1,reg],primary_channel]
	# subdata = data[regions[0,reg]:regions[1,reg]]
	smoothing(subdata, 5)
	current_subplot = (reg//4, reg%4) #if reg!=regions.shape[1]-1 else (3, 9)
	ax[current_subplot[0], current_subplot[1]].plot(subdata) 
	y=np.abs(scipy.fft.fft(subdata))
	spectrum[reg] = y[0:subdata.shape[0]//2]
	ax1[current_subplot[0],current_subplot[1]].plot(x, spectrum[reg])
	# amplitudes = [3e6]
	for peak in range(peaks.shape[1]):
		try:
			left, right = round(peaks[0, peak]/koef), round(peaks[1, peak]/koef)
			max_ind = np.argmax(spectrum[reg][left:right]) + left 
			fit_parameters = np.array([spectrum[reg][max_ind], max_ind*koef, 30, 0, spectrum[reg][max_ind]-70])
			# fit_parameters = np.array([3e6, max_ind*koef, 14, 0, 0.2e7 ])
			# print("Start point ", max_ind*koef, "---", left*koef,right)
			popt, pcov = curve_fit(Gauss, x[left:right], spectrum[reg][left:right], p0=fit_parameters)
			ax1[current_subplot[0],current_subplot[1]].plot(x[left:right], Gauss(x[left:right], *popt), 'r-')
			if popt[1]< left*koef or popt[1]> right*koef: popt[0] = 0
			spectrum_fit_param[reg][peak] = popt
		except Exception:
			print("FATAL" , peak, reg)
			pass
	ax1[current_subplot[0],current_subplot[1]].set_title(reg*10)

print(spectrum_fit_param[:,3,:])
degrees = np.arange(0, 370, 10)

ampl = np.empty([peaks.shape[1], degrees.shape[0]])
fig,ax = plt.subplots(2, 2) 
for quarter in range(4):
	for reg in range(10):
		if 10*quarter+reg == 37: break
		ax[quarter//2,quarter%2].plot(x[:6000], spectrum[10*quarter+reg][:6000])
		for i in range(peaks.shape[1]): 
			ampl[i,10*quarter+reg] = spectrum_fit_param[10*quarter+reg][i][0]
			# ampl[i,10*quarter+reg] = np.amax(spectrum[10*quarter+reg][round(peaks[0][i]/koef):round(peaks[1][i]/koef)])
	ax[quarter//2,quarter%2].legend([str(degree) for degree in degrees[quarter*10:10*quarter+reg+1]])
	# ax[quarter//2,quarter%2].set_yscale("log")
# for i in range(peaks.shape[1]): print(i, ampl[i]/1e7)

fig,ax = plt.subplots(peaks.shape[1]//2+1, 2) 
for peak in range(peaks.shape[1]):
	ax[peak%(peaks.shape[1]//2+1)][round(2*peak//(peaks.shape[1]+1))].plot(degrees, ampl[peak])
	
	
plt.show()