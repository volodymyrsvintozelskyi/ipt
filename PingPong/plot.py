import matplotlib
import matplotlib.pyplot as plt
import scipy.fft
from scipy.io import wavfile
from scipy.optimize import curve_fit
import numpy as np


data0 = np.array([[87.8, 49.3, 40.2, 27.9, 20.9], [81.8, 57.1, 37.8], [85.4, 57.5], [73.3], [91.1, 61.4], [87.5, 60.4, 39.3], [92.9, 63.0, 35.1],[88.3, 60.6],[85.2],[88.0, 61.4],[81.0],[94.9, 62.6]], dtype =object)
data3 = np.array([[ 70.6, 8.7], [70.1], [85.4, 13.7], [94.9, 7.8], [61.8], [81.7, 7.3], [70.2, 8.3], [50.5],  [6.2]], dtype =object)
data6 = np.array([[104.3, 8.5], [102.6, 7.37], [106.8, 9.32], [90.5]], dtype =object)
data9 = np.array([[57.7, 6.9], [75.1, 8.6], [106.4], [19.3], [8.7], [83.5, 9.5], [93.8, 9.3], [101.3, 9.5], [61.9], [100.6, 7.9]], dtype =object)
data12 = np.array([[42.2, 3.67], [102.2, 4.7], [52.1, 4.95], [34.7, 2.97], [91.2], [30.9], [77.8], [54.1, 5.5], [33.9, 5.2], [69.7, 5.9], [64.6], [65.3], [39.8, 5.9], [62.9, 4.47], [40.8, 5.14], [53.2], [58.4], [72.1], [62.6]], dtype =object)
data15 = np.array([[61.4, 5.65], [35.7], [26.4], [62.9], [27.9], [17.1], [78.7, 5.1], [19.6], [55.8], [39.3, 4.2], [77.0, 9.75], [14.4, 1.35], [35.3, 3.7], [41.6, 5.67], [84.2, 7.3], [15.5], [39.8], [58.8], [30.5, 1.75], [75.0, 9.6]], dtype =object)
data18 = np.array([[20.8, 1.9], [51.6], [15.6], [41.9, 3.8], [37.1], [18.9], [14.1, 1.7], [23.5, 3.76], [42.6], [25.3], [46.2, 3.59], [37.0, 4.5], [46.0, 1.5], [32.4], [46.1], [40.4, 4.5], [25.7], [29.8, 3.0], [50.3, 3.4], [10.7], [11.3]], dtype =object)
data21 = np.array([[32.3], [23.6, 3.1], [15.5, 3.5], [37.1, 4.4], [44.9, 9.0], [21.5], [20.3]], dtype =object)
data24 = np.array([[31.1, 3.5], [28.5, 4.0], [23.0, 3.08], [28.4, 3.89], [45.4, 8.27], [21.6, 5.3], [33.1, 6.8], [10.9, 2.8], [28.0, 6.3], [41.0, 7.9], [23.3, 5.1], [43.7, 9.0], [54.8, 8.95], [13.5, 2.8]], dtype =object)
data26 = np.array([[41.3, 6.74], [29.5, 6.7], [37.5, 7.4], [30.5, 7.65], [20.0, 5.18], [48.8, 7.0], [26.6, 8.0]], dtype =object)
data28 = np.array([[39.8, 14.9], [51.8, 18.3], [60.3, 17.3], [44.8, 13.8], [39.6, 11.5], [33.6, 11.0], [39.6, 15.1], [31.2, 9.12], [35.8, 12.2], [42.3, 11.9], [44.6, 12.2], [39.0, 10.9]], dtype =object)
data31 = np.array([[58.1, 31.4], [78.3, 37.5], [70.6, 30.3], [69.4, 38.0], [73.3, 41.1, 18.4], [45.1, 21.1]], dtype =object)
data = [data0, data3, data6, data9, data12, data15, data18, data21, data24, data26, data28, data31]


filled = np.array([0, 3, 6, 9, 12, 15, 18, 21, 24, 26,28, 31])
filled = filled/31


first_jump = np.empty(filled.shape[0])
first_jump_err = np.empty(filled.shape[0])
second_jump = np.empty(filled.shape[0])
second_jump_err = np.empty(filled.shape[0])

second_to_first = np.empty(filled.shape[0])
second_to_first_err = np.empty(filled.shape[0])

num_of_values = 3


def find_avg_height(data, jump, num_of_values):
	arr = np.sort([i[jump] for i in data if len(i)>jump])	
	max_values=arr[-1*num_of_values :]
	avg = np.sum(max_values)/num_of_values
	return avg, 3*np.sqrt(np.sum([(avg-i)*(avg-i) for i in max_values ])/num_of_values)

def find_relativeness(data, title):
	index_array = [i for i in range(data.shape[0]) if(len(data[i])>1)]
	first_jump = np.array([data[i][0] for i in index_array])
	second_jump = np.array([data[i][1] for i in index_array])
	fig,ax = plt.subplots(1) 
	ax.scatter(first_jump, second_jump/first_jump)
	ax.set_title(title)
	# ax.errorbar(filled, second_jump, yerr = second_jump_err, fmt='o',ecolor = 'cyan',color='black')

for exp in range(filled.shape[0]):	
	first_jump[exp], first_jump_err[exp] = find_avg_height(data[exp], 0, num_of_values) 
	second_jump[exp], second_jump_err[exp] = find_avg_height(data[exp], 1, num_of_values)
	# find_relativeness(data[exp], str(filled[exp]))
	# second_to_first[exp], second_to_first_err[exp]  = find_relativeness(data[exp])

	
fig,ax = plt.subplots(1) 
ax.scatter(filled*100, first_jump)
ax.errorbar(filled*100, first_jump, yerr = first_jump_err, fmt='o',ecolor = 'cyan',color='black')
ax.set_xlabel("Кількість води, %")
ax.set_ylabel("Висота відскоку шарика, см")
fig,ax = plt.subplots(1) 
ax.scatter(filled*100, second_jump)
ax.errorbar(filled*100, second_jump, yerr = second_jump_err, fmt='o',ecolor = 'cyan',color='black')
ax.set_xlabel("Кількість води, %")
ax.set_ylabel("Висота відскоку шарика, см")
# fig,ax = plt.subplots(1) 
# ax.scatter(filled, second_to_first)
# ax.errorbar(filled, second_to_first, yerr = second_to_first_err, fmt='o',ecolor = 'cyan',color='black')

fig,ax = plt.subplots(1) 
values = [data24[i][0] for i in range(data0.shape[0])]
ax.hist(values)
# print(values)

plt.show()